EmmyAnalytics
========================
To run emmy analytics on your environment you should have next components:
0. Python 3.5
1. RabbitMQ - installed and up
2. MongoDb - installed and up
3. Hadoop - installed and up
4. Spark - installed and up
5. Mongo-hadoop - installed (https://github.com/mongodb/mongo-hadoop/blob/master/spark/src/main/python/README.rst)


When this components installed you need to format hdfs and create next folder:
/user/emmy/input/en
/user/emmy/input/ru

Then run preparation.sh script to install all python packages
New colelction job can be added with script /tools/first_run.py


HDFS tweets location:
/user/emmy/input/en/{date}/tweets.txt
