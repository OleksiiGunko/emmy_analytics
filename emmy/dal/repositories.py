import logging

from bson.objectid import ObjectId
from pymongo import MongoClient
from emmy.common.enums import ConnectorStatus
from emmy.common.models import ConnectorsRepository, ConnectorJobRepository, ConnectorJob, Connector, \
    AnalyticJobRepository, AnalyticJob
from emmy.utils.config import ConfigManager


class DbResearchObjectsRepository():
    """
    DB interface for Research object
    """
    def __init__(self):
        self.logger = logging.getLogger("DbResearchObjectsRepository")
        self.logger.debug('DbResearchObjectsRepository() - object created')
        self.mongo_client = None
        self.config_mngr = ConfigManager()
        self.mongo_uri = self.config_mngr.get_mongo_uri()
        self.db = None
        self.isConnected = False

    def add(self, r_object):
        """
        adding new research object in collection
        :param r_object:
        :return:
        """
        self.logger.debug('add() - call for %s', r_object.name)
        try:
            self.__connect_db()
            object_collection = self.db.research_objects

            obj_id = object_collection.insert_one({
                'name': r_object.name,
                'twitter_search': r_object.twitter_search,
                'facebook_search': r_object.facebook_search,
                'search_twitter': r_object.search_keywords
            }).inserted_id
            self.logger.info('add_or_update() - inserted %s', obj_id)

            return obj_id
        except Exception as exc:
            self.logger.error('add_or_update() - error occured - %s', exc)
            return None
        finally:
            self.__close_db()

    def get_all(self):
        self.logger.debug('get_all() - called')

    def __connect_db(self):
        if self.isConnected is False:
            self.mongo_client = MongoClient(self.mongo_uri)
            self.db = self.mongo_client['emmy_db']
            self.isConnected = True

    def __close_db(self):
        if self.isConnected:
            self.mongo_client.close()
            self.isConnected = False


class DbConnectorsRepository(ConnectorsRepository):
    """
    Connector DAO object for Mongo DB
    """

    def __init__(self):
        self.logger = logging.getLogger("DbConnectorsRepository")
        self.logger.debug('DbConnectorsRepository() - object created')
        self.mongo_client = None
        self.config_mngr = ConfigManager()
        self.mongo_uri = self.config_mngr.get_mongo_uri()
        self.db = None
        self.isConnected = False

    def add_or_update(self, hostname):
        """
        Adding connector or updates status if its exists
        :param hostname: connector hostname
        :return: id or None if error occured
        """
        self.logger.debug('add_or_update() - adding %s', hostname)
        try:
            self.__connect_db()
            connectors = self.db.connectors

            # first trying to find existing
            existing_obj = connectors.find_one({"hostname": hostname})
            if existing_obj is None:
                connector_id = connectors.insert_one(
                    {'hostname': hostname, 'status': ConnectorStatus.READY}).inserted_id
                self.logger.info('add_or_update() - inserted %s', hostname)
            else:
                connectors.update_one({
                    'hostname': hostname
                }, {'$set': {
                    'status': ConnectorStatus.READY
                }})
                connector_updated = connectors.find_one({'hostname': hostname})
                connector_id = connector_updated['_id']

            return connector_id
        except Exception as exc:
            self.logger.error('add_or_update() - error occured - %s', exc)
            return None
        finally:
            self.__close_db()

    def update_status(self, hostname, status):
        """
        Updating status of existing connector
        :param hostname: Connector hostname
        :param status: conenctor status
        :return: True if ok, None - if not
        """

        self.logger.debug('update_status() - updating for %s status: %s', hostname, status)
        try:
            self.__connect_db()
            connectors = self.db.connectors

            connectors.update_one({
                'hostname': hostname
            }, {
                '$set': {'status': status}
            })

            return True
        except Exception as exc:
            self.logger.error('update_status() - cant update status: %s', exc)
            return None
        finally:
            self.__close_db()

    def get_all_ready(self):
        self.logger.debug('get_all_read() - called')
        try:
            self.__connect_db()
            connectors = []
            connectors_res = self.db.connectors.find({'status': '1'})

            for connector_res in connectors_res:
                buffer_connector = Connector()
                buffer_connector.id = connector_res['_id']
                buffer_connector.hostname = connector_res['hostname']
                buffer_connector.status = connector_res['status']
                connectors.append(buffer_connector)

            self.logger.info('get_all_ready() - %s ready connectors founded', len(connectors))

            return connectors
        except Exception as exc:
            self.logger.error('get_all_ready() - error: %s', exc)
            return None

    def __connect_db(self):
        if self.isConnected is False:
            self.mongo_client = MongoClient(self.mongo_uri)
            self.db = self.mongo_client['emmy_db']
            self.isConnected = True

    def __close_db(self):
        if self.isConnected:
            self.mongo_client.close()
            self.isConnected = False


class DbJobRepository(ConnectorJobRepository):
    """
    DAO object for jobs
    """

    def __init__(self):
        self.logger = logging.getLogger('DbJobRepository')
        self.mongo_client = None
        self.config_mngr = ConfigManager()
        self.mongo_uri = self.config_mngr.get_mongo_uri()
        self.db = None
        self.isConnected = False

    def get_connector_jobs(self):
        """
        Returns list of all jobs from db
        :return:
        """
        self.logger.debug('get_connector_jobs() - called')
        try:
            self.__connect_db()
            all_jobs_res = self.db.connector_jobs.find()
            jobs = []

            for job_res in all_jobs_res:
                new_job = ConnectorJob()
                new_job.id = str(job_res['_id'])
                new_job.filter = job_res['filter']
                new_job.language = job_res['language']
                new_job.interval = job_res['interval']
                new_job.lastRun = job_res['lastRun']
                new_job.type = job_res['type']
                new_job.research_object_id = str(job_res['research_object_id'])
                jobs.append(new_job)

            return jobs
        except Exception as exc:
            self.logger.error('get_jobs() - error: %s', exc)
            return None
        finally:
            self.__close_db()

    def update_job(self, job):
        """
        Updating job in db
        :param job: job to update
        :return: None if error
        """
        try:
            self.logger.debug('update_job() - updating job %s', job.id)
            self.__connect_db()
            connector_jobs = self.db.connector_jobs

            connector_jobs.update_one({
                '_id': ObjectId(job.id)
            }, {
                '$set': {'lastRun': job.lastRun}
            })

        except Exception as exc:
            self.logger.error('update_job() - error: %s', exc)
            return None
        finally:
            self.__close_db()

    def __connect_db(self):
        """
        Connecting to mongo db
        :return:
        """
        if self.isConnected is False:
            self.mongo_client = MongoClient(self.mongo_uri)
            self.db = self.mongo_client['emmy_db']
            self.isConnected = True

    def __close_db(self):
        """
        Closing connection with mongo db
        :return:
        """
        if self.isConnected:
            self.mongo_client.close()
            self.isConnected = False


class DbAnalyticJobRepository(AnalyticJobRepository):
    """
    Contains logic for CRUD operations with analytics jobs
    """

    def __init__(self):
        self.logger = logging.getLogger('DbJobRepository')
        self.mongo_client = None
        self.config_mngr = ConfigManager()
        self.mongo_uri = self.config_mngr.get_mongo_uri()
        self.db = None
        self.isConnected = False

    def add_job(self, job):
        """
        Adding new analytics job in db
        :param job:
        :return:
        """
        try:
            self.__connect_db()
            jobs_collection = self.db.analytic_jobs

            job_id = jobs_collection.insert_one(
                {
                    'name': job.name,
                    'status': job.status,
                    'language': job.language,
                    'input_folder': job.input_folder,
                    'type': job.type,
                    'connector_type': job.connector_type,
                    'research_object_id': ObjectId(job.research_object_id)
                }).inserted_id

            return job_id
        except Exception as exc:
            self.logger.error('add_or_update() - error occured - %s', exc)
            return None
        finally:
            self.__close_db()

    def get_all(self):
        """
        Returns list of all jobs
        :return:
        """
        self.logger.debug('get_all() - called')

        jobs = []
        try:
            self.__connect_db()
            jobs_collection = self.db.analytic_jobs.find()

            for job_dict in jobs_collection:
                job = AnalyticJob()
                job.id = str(job_dict['_id'])
                job.input_folder = job_dict['input_folder']
                job.connector_type = job_dict['connector_type']
                job.language = job_dict['language']
                job.name = job_dict['name']
                job.status = job_dict['status']
                job.type = job_dict['type']
                jobs.append(job)

        except Exception as exc:
            self.logger.error('get_all() - error: %s', exc)
            return None
        finally:
            self.__close_db()
            return jobs

    def update(self, job):
        self.logger.debug('update() - called')

        jobs = []
        try:
            self.__connect_db()
            jobs_collection = self.db.analytic_jobs
            jobs_collection.update_one({
                '_id': ObjectId(job.id)
            }, {
                '$set': {
                    'status': job.status,
                    'type': job.type
                }
            })
            return True
        except Exception as exc:
            self.logger.error('get_all() - error: %s', exc)
            return None
        finally:
            self.__close_db()
            return jobs

    def __connect_db(self):
        """
        Connecting to mongo db
        :return:
        """
        if self.isConnected is False:
            self.mongo_client = MongoClient(self.mongo_uri)
            self.db = self.mongo_client['emmy_db']
            self.isConnected = True

    def __close_db(self):
        """
        Closing connection with mongo db
        :return:
        """
        if self.isConnected:
            self.mongo_client.close()
            self.isConnected = False
