import logging

from twitter import OAuth
from twitter import Twitter

from emmy.common.models import TwitterClient
from emmy.utils.config import ConfigManager

class TwitterSearchClient(TwitterClient):
    """
    Responsible for commutation with Twitter API
    """

    def __init__(self):
        self.logger = logging.getLogger("TwitterApiClient")
        self.config_manager = ConfigManager.instance
        self.access_token = None
        self.access_secret = None
        self.consumer_key = None
        self.consumer_secret = None
        self.twitter = None

    def connect(self):
        """
        Connecting to Twitter API
        :return: True if Ok
        """
        try:

            self.access_token = self.config_manager.get_access_token()
            self.access_secret = self.config_manager.get_access_secret()
            self.consumer_key = self.config_manager.get_consumer_key()
            self.consumer_secret = self.config_manager.get_consumer_secret()
            self.oauth = OAuth(self.access_token, self.access_secret, self.consumer_key, self.consumer_secret)
            return True
        except Exception as exc:
            self.logger.error('connect() - error occured %s', exc)
            return False

    def get_tweets_array(self, search_filter, count=100, lang="en", result_type='recent'):
        """
        Getting all tweets with selected filter
        :param search_filter: twitter filter text
        :param count: number of tweets
        :param lang: language of tweet
        :param result_type: type of tweets
        :return: list of tweets in JSON
        """
        self.logger.info('get_tweets_array() - filter: %s, count = %s, lang=%s', search_filter, count, lang)
        try:
            self.twitter = Twitter(auth=self.oauth)
            search_result = self.twitter.search.tweets(q=search_filter, lang=lang, count=count, result_type=result_type)

            # result contains statuses and search metadata, we taking only statuses
            return search_result['statuses']
        except Exception as exc:
            self.logger.warning('get_tweets_array() - can not get: %s', exc)
            return None
