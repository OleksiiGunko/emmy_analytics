import logging
import os
import threading

import time

import datetime
from subprocess import call

from emmy.common.enums import ConnectorStatus, JobStatus, AnalyticJobType
from emmy.common.models import JobManager
from emmy.utils.config import ConfigManager


MONGO_PACKAGE = '--jars ./dependencies/mongo-hadoop-spark.jar --driver-class-path ./dependencies/mongo-hadoop-spark.jar'

class JobManagerService(JobManager):
    """
    Adding new jobs for all workers and managing
    them in database
    """
    def __init__(self):
        self.job_worker_thread = None
        self.ajob_worker_thread = None
        self.isStopped = True
        self.config_manager = ConfigManager()
        self.logger = logging.getLogger('JobManagerService')
        self.logger.debug('object created')

    def start(self):
        self.logger.debug('start() - starting receiver worker')
        self.isStopped = False
        self.job_worker_thread = threading.Thread(target=self.__cjob_worker)
        self.job_worker_thread.start()

        self.ajob_worker_thread = threading.Thread(target=self.__ajob_worker)
        self.ajob_worker_thread.start()

    def stop(self):
        self.logger.debug('stop() - stopping worker')
        self.isStopped = True

    def __ajob_worker(self):
        """
        Worker for handling analytic jobs
        :return: None
        """
        self.logger.info('ajob_worker() - started')
        while self.isStopped is False:
            try:
                ajobs = self.ajobs_repository.get_all()
                if ajobs is None:
                    self.logger.debug('ajob_worker() - no jobs, wait for 60 sec')
                    time.sleep(60)

                for job in ajobs:
                    self.logger.debug('ajob_worker() - job: %s', job.id)
                    if job.status == JobStatus.PENDING and job.type == AnalyticJobType.SENTIMENT_ANALYTICS:
                        self.__start_analytic_job(job)
            except Exception as exc:
                self.logger.error('ajob_worker() - cant work - %s', exc)
            time.sleep(60)

    def __cjob_worker(self):
        """
        Starts new jobs if time interval from last run is passed
        :return:
        """
        self.logger.info('job_worker() - started')

        # checking jobs to run with 1 minute interval
        while self.isStopped is False:
            self.logger.debug('job_worker() - checking statuses')
            jobs = self.cjobs_repository.get_connector_jobs()
            while jobs is None:
                self.logger.debug('job_worker() - no jobs to process, wait for 60 sec')
                time.sleep(60)
                jobs = self.cjobs_repository.get_connector_jobs()

            for job in jobs:
                self.logger.debug('job_worker() - checking job %s', job.id)
                # getting next run time by adding interval in seconds to last run
                should_run_time = job.lastRun + datetime.timedelta(0, job.interval)
                if should_run_time<datetime.datetime.now():
                    self.__send_connector_job(job)

            time.sleep(60)

    def __send_connector_job(self, job):
        self.logger.info('send_connector_job() - checking job %s', job.id)
        # getting free connector and sending job to it
        ready_connectors = self.connector_repository.get_all_ready()
        while len(ready_connectors) < 1:
            self.logger.warning('send_connector_job() - not ready connector')
            time.sleep(10)
            ready_connectors = self.connector_repository.get_all_ready()

        connector = ready_connectors[0]
        try:
            job.connector_job = connector.hostname
            self.com_client.send_connector_job(job)
            self.connector_repository.update_status(connector.hostname, ConnectorStatus.BUSY)

            job.lastRun = datetime.datetime.now()
            self.cjobs_repository.update_job(job)
        except Exception as exc:
            self.logger.error('send_connector_job() - error: %s', exc)

    def __start_analytic_job(self, job):
        self.logger.debug('start_analytics_job() - called for %s', job.name)

        # building command for spark execution
        # notice: your models should be located in {spark}/models
        spark_folder = self.config_manager.get_spark_location()

        # example: /home/emmy/spark/bin/spark-submit
        spark_program = "{}/bin/spark-submit".format(spark_folder)

        ###########################################################
        # expected arguments list:
        # 1. spark info (for local: --master local[2])
        # 2. script name
        # 3. input file or folder
        # 4. research object id
        # 5. output folder
        # 6. type
        # 7. language
        ###########################################################
        script_name = ""
        if job.language == 'en':
            script_name = 'predict_sent.py'
        if job.language == 'ru':
            script_name = 'predict_sent_ru.py'

        output_folder = self.config_manager.getOutputFolder()
        mongo_uri = self.config_manager.get_mongo_uri()

        script_name_arg = "./emmy/spark_jobs/sent_analytics/{}".format(script_name)
        inp_folder_arg = job.input_folder
        res_obj_arg = str(job.research_object_id)
        out_folder_arg = output_folder
        type_arg = str(job.type)
        lang_arg = str(job.language)

        os.environ['PYTHONPATH'] = self.config_manager.get_monhadoop_folder()
        os.environ['PYSPARK_PYTHON'] = 'python3'

        try:
            self.logger.info('starting spark job')
            # sending job to spark
            call([spark_program,script_name_arg, inp_folder_arg, res_obj_arg, out_folder_arg, type_arg,lang_arg, mongo_uri])

            # updating job in db
            job.status = JobStatus.DONE
            self.ajobs_repository.update(job)
        except Exception as exc:
            self.logger.error('send_analytic_job() - error: %s', exc)
