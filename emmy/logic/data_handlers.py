import logging

import datetime
import os
from threading import Thread

import gc
from pywebhdfs.webhdfs import PyWebHdfsClient

from emmy.common.enums import JobStatus, AnalyticJobType
from emmy.common.models import DataReceiver, AnalyticJob
from emmy.utils.config import ConfigManager


class DbDataReceiver(DataReceiver):
    """
    Class receives data from comm clients and saves it in
    appropriate storage
    """
    def __init__(self):
        self.config_manager = ConfigManager()
        self.logger = logging.getLogger('DvDataReceiver')
        self.data_folder = self.config_manager.getDataFolder()
        self.hadoop_client = None
        self.load_data_worker = None
        self.logger.debug('object created')

    def start(self):
        """
        Starting to listen data and save it to local fs and HDFS
        :return:
        """
        self.logger.debug('start() - starting receiver worker')
        self.com_client.set_tweets_processor(self.__on_tweet_received)

        self.hadoop_client = PyWebHdfsClient(user_name=self.config_manager.getHadoopUser(),
                                             host=self.config_manager.getHadoopHost(),
                                             port=self.config_manager.getHadoopPort())

    def stop(self):
        self.logger.debug('stop() - stopping worker')
        self.com_client.set_tweets_processor(None)

    def read_local_dir(local_path):
        for fn in os.listdir(local_path):
            path = os.path.join(local_path, fn)
            if os.path.isfile(path):
                yield path, open(path).read()

    def __on_tweet_received(self, tweet_text):
        """
        This method is callback for received tweets
        When new tweet arrived we checking if we have folder with today date
        If no: start loading yesterday data folder to hdfs
        After that saving new tweet in tweet text file
        :param tweet_text: text of loaded tweet
        :return: None
        """
        self.logger.debug('on_tweet_received() - %s received', tweet_text)
        try:
            folder_name = "{0}/en/{1}".format(self.data_folder, datetime.datetime.now().date())
            job_id = tweet_text.split()[0]

            # TODO: change to filter
            all_jobs = self.cjob_repository.get_connector_jobs()

            for job in all_jobs:
                if job.research_object_id == job_id:
                    cjob = job

            if os.path.isdir(folder_name) is False:
                self.logger.info('on_tweet_received() - no active data folder')
                os.makedirs(folder_name)

                self.load_data_worker = Thread(target=self.__process_ready_folder,
                                               name='process_ready_folder',
                                               args=(cjob,))
                self.load_data_worker.start()

            file_name = "{0}/{1}_tweets.txt".format(folder_name, cjob.research_object_id)

            with open(file_name,'a') as f:
                # getting tweet json without job name
                f.write(tweet_text[len(job_id)+1:] + '\n')

        except Exception as exc:
            self.logger.error('on_tweet_received() - error occured: %s', exc)

    def __process_ready_folder(self, job):
        """
        This method should be called once a day
        It loads data from local FS to HDFS using PyWebHdfs
        :return:
        """
        self.logger.debug('process_ready_folder() - loading data in storage for job %s', job)
        previous_date = datetime.datetime.now().date() - datetime.timedelta(1)

        folder_to_load = "{}/{}/{}".format(self.data_folder, job.language, previous_date)
        local_file_name = "{}/{}_tweets.txt".format(folder_to_load, job.research_object_id)

        hdfs_path = "/user/emmy/input/en/{}".format(datetime.datetime.now().date() - datetime.timedelta(1))
        hdfs_file_name = "{}/{}_tweets.txt".format(hdfs_path, job.id)

        if os.path.isdir(folder_to_load):
            self.logger.info('process_ready_folder() - loading folder %s', folder_to_load)
            try:
                use_hdfs = self.config_manager.use_hdfs()
                if use_hdfs:
                    self.hadoop_client.make_dir(hdfs_path)

                    with open(local_file_name, 'r') as content_file:
                        content = content_file.read()

                    self.hadoop_client.create_file(hdfs_file_name, content)
                    self.__add_analytics_job(job, hdfs_file_name)
                    os.rmdir(folder_to_load)
                else:
                    self.__add_analytics_job(job, local_file_name)

                gc.collect()
            except Exception as exc:
                self.logger.error('process_ready_folder() - cant load folder: %s', exc)

    def __add_analytics_job(self, connector_job, input_file):
        """
        Adding new analytic job for handling
        :param connector_job: Connector job to process
        :param input_folder: folder where input exists
        :return:
        """
        self.logger.debug('add_analytics_jobs() - adding analytics job for %s', connector_job.research_object_id)
        try:
            ajob = AnalyticJob()
            ajob.research_object_id = connector_job.research_object_id
            ajob.language = connector_job.language
            ajob.input_folder = input_file
            ajob.status = JobStatus.PENDING
            ajob.connector_type = connector_job.type
            ajob.type = AnalyticJobType.SENTIMENT_ANALYTICS

            self.ajob_repository.add_job(ajob)
        except Exception as exc:
            self.logger.error('add_analytics_job() - cant add job in repository: %s', exc)

