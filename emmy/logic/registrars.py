import datetime
import logging
import threading
import time

from emmy.common.models import Registrar
from emmy.utils.config import ConfigManager


class RegistrarService(Registrar):
    """
    Class responsible for registration of system
    components in database
    """
    def __init__(self):
        self.isActive = True
        self.keep_alive_worker = None
        self.config_manager = ConfigManager()
        self.logger = logging.getLogger('RegistrarService')
        self.logger.debug('object created')

        # this object will contain information
        # with last connector status
        self.connectors_timer = {}
        self.status_locker = threading.Lock()

    def __on_registration_received(self, hostname):
        """
        Callback for registration
        This realisation is sync
        :return:
        """
        self.logger.info('on_registration_received() - request received for: %s', hostname)
        try:
            if self.connectors_rep.add_or_update(hostname.decode('utf-8')) is not None:
                self.com_client.send_reg_response(hostname.decode('utf-8'))
            else:
                self.logger.warning('on_registration_received() - cant save connector')
        except Exception as exc:
            self.logger.error('on_registration_received() - error: %s', exc)

    def __on_status_received(self, hostname, status):
        """
        Receives connector status and updates it
        :param hostname: Hostname of connector to update
        :param status: Status of connector
        :return: None
        """
        self.logger.debug('on_status_received() - called for %s', hostname)
        try:
            self.connectors_rep.update_status(hostname, status)
            self.status_locker.acquire()
            self.connectors_timer[hostname] = datetime.datetime.now()
            self.status_locker.release()
        except Exception as exc:
            self.logger.error('on_status_received() - error: %s', exc)

    def __connector_status_worker(self):
        self.logger.debug('connector_status_worker() - started')
        while self.isActive:
            self.status_locker.acquire()
            for key, last_st_update in self.connectors_timer.items():
                self.logger.debug('connector status checker() - checking %s : %s', key, last_st_update)
                # last update should be < 20 seconds
                expected_delta = last_st_update + datetime.timedelta(0,20)
                if datetime.datetime.now() > expected_delta:
                    self.logger.warning('connector_status_worker() - no status')
                    self.connectors_rep.update_status(key, '0')
            self.status_locker.release()
            time.sleep(10)

    def start(self):
        self.logger.debug('start() - starting receiver worker')
        self.com_client.set_regreq_callback(self.__on_registration_received)
        self.com_client.set_keep_alive_processor(self.__on_status_received)

        # stating timer validation for keep alive checking
        self.keep_alive_worker = threading.Thread(target=self.__connector_status_worker)
        self.keep_alive_worker.start()

        self.isActive = True

    def stop(self):
        self.logger.debug('stop() - stopping worker')
        self.isActive = False