import logging

from emmy.common.models import AdminService
from emmy.utils.config import ConfigManager


class CoreAdminService(AdminService):
    """
    This main infrastructure service. It's responsible for registration of
    connectors, managing of jobs and saving data from connectors to DB
    """
    def __init__(self):
        self.config_manager = ConfigManager()
        self.logger = logging.getLogger('CoreAdminService')
        self.logger.debug('object created')

    def start(self):
        """
        Starting all workers for registration of components,
        job managing and data receiving
        :return: True if ok
        """
        self.logger.debug('start() - starting service')

        try:
            # 0 - init for comm client
            self.com_client.start()

            # 1. Start worker thread for registration
            self.registrar.start()
            self.logger.debug('start() - registrar started')

            # 2. Starting Job Manager worker
            self.job_manager.start()
            self.logger.debug('start() - job manager started')

            # 3. Starting data receiver worker
            self.data_receiver.start()
            self.logger.debug('start() - job datareceiver started')

            self.logger.info('start() - administration started')
        except Exception as exc:
            self.logger.error('start() - error occurred: %s', exc)
            return False

    def stop(self):
        """
        Stopping all managed workers
        :return:
        """
        self.logger.debug('stop() - stopping service')
        try:
            self.registrar.stop()
            self.job_manager.stop()
            self.data_receiver.stop()

            return True
        except Exception as exc:
            self.logger.error('stop() - error occurred: %s', exc)
            return False