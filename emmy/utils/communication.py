import gc
import json
import logging
import threading

import pika
from bson import json_util

from emmy.common.models import CommunicationClient, ConnectorJob
from emmy.utils.config import ConfigManager

TWEETS_QUEUE = "tweets_queue"

CONNECTOR_JOB_QUEUE = "connector_job_queue"

KEEP_ALIVE_QUEUE = "keep_alive"

REGISTRATION_RESPONSE_QUEUE = 'registration_response'

REGISTRATION_QUEUE_NAME = "registration"


class RabbitCommunicationClient(CommunicationClient):
    """
    RabbitMQ client for messages exchange
    """

    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.configManager = ConfigManager()
        self.rabbitmq_host = self.configManager.get_rabbitmq_host()
        self.connection = None
        self.channel = None
        self.register_processor = None
        self.job_processor = None
        self.register_request_processor = None
        self.logger.info('init() - created for %s rabbit mq', self.rabbitmq_host)
        self.keep_alive_processor = None
        self.start_lock = threading.Lock()
        self.tweets_processor = None

    def start(self):
        self.logger.debug('start() - method called')
        self.__connect()

    def set_regresp_callback(self, processor):
        self.logger.info('start_listening() - called')
        self.register_processor = processor

    def set_regreq_callback(self, processor):
        self.logger.info('set_regreq_callback() - called')
        self.register_request_processor = processor

    def set_keep_alive_processor(self, processor):
        self.logger.info('set_keep_alive_processor() - called')
        self.keep_alive_processor = processor

    def set_tweets_processor(self, processor):
        self.logger.info('set_tweets_processor() - called')
        self.tweets_processor = processor

    def start_job_listening(self, processor):
        self.logger.debug('start_job_listening() - listening for job')
        self.job_processor = processor
        self.__connect()

    def send_connector_status(self, hostname, status):
        self.logger.debug('send_status() - sending node status')
        self.__connect()

        # keep alive message contains node type, hostname and status
        message = "connector {} {}".format(hostname, status)

        self.channel.basic_publish(exchange='',
                                   routing_key=KEEP_ALIVE_QUEUE,
                                   body=message)
        self.logger.info('send_connector_status() - message sent %s', message)

    def send_reg_response(self, hostname):
        self.logger.debug('send_request_response() - called for host: %s', hostname)
        self.channel.basic_publish(exchange='',
                                   routing_key=REGISTRATION_RESPONSE_QUEUE,
                                   body=hostname)
        self.logger.info('send_request_response() - response sent for %s', hostname)

    def stop(self):
        self.logger.info('stop_listening() - called')
        if self.connection is not None:
            self.connection.close()
            self.connection = None
            gc.collect()

    def send_register_connector(self, hostname):
        """
        Sending registration message for connector to admin
        :param hostname: hostname of connector
        :return: True if sent
        """
        self.logger.debug('send_register() - sending message %s', hostname)
        try:
            self.channel.basic_publish(exchange='',
                                       routing_key=REGISTRATION_QUEUE_NAME,
                                       body=hostname)
            self.logger.info('send_register_connector() - message sent')
            return True
        except Exception as exc:
            self.logger.error('send_register_connector() - error: %s', exc)
            return False

    def send_connector_job(self, job):
        self.logger.debug('send_connector_job() - sending job')

        message = self.__job_to_json(job)
        self.channel.basic_publish(exchange='',
                                   routing_key=CONNECTOR_JOB_QUEUE,
                                   body=message)
        self.logger.info('send_connector_job() - job %s sent', message)

    def send_collected_tweets(self, job, tweets):
        """
        Sends tweets to admin service
        :param tweets: list with tweets in JSON
        :return: True if sent
        """
        try:
            self.__connect()
            self.channel.queue_declare(queue=TWEETS_QUEUE)
            self.logger.info('send_collected_tweets() - %s messages to send', len(tweets))
            for str in tweets:
                json_str = json.dumps(str, ensure_ascii=False)
                tweet_message = "{0} {1}".format(job, json_str)
                self.channel.basic_publish(exchange='',
                                           routing_key=TWEETS_QUEUE,
                                           body=tweet_message)

            self.logger.info('send_collected_tweets() - tweets was sent')
            return True
        except Exception as exc:
            self.logger.error('send_collected_tweets() - error occured: %s', exc)
            return False

    def __register_callback(self, ch, method, properties, body):
        self.logger.debug('callback() - message: %s, ch: %s', body, ch)
        if self.register_processor is not None:
            self.register_processor(body)

    def __job_callback(self, ch, method, properties, body):
        self.logger.debug('job_callback() - method: %s, message %s', method, body)
        if self.job_processor is not None:
            # decoding to json
            json_message = json.loads(body.decode('utf-8'))
            # parsing to object
            job_instance = self.__parse_job(json_message)
            self.job_processor(job_instance)

    def __keep_alive_callback(self, ch, method, properties, body):
        self.logger.debug('keep_alive_callback() - called for %s', body)
        if self.keep_alive_processor is not None:
            words = body.decode('utf-8').split()
            self.keep_alive_processor(words[1], words[2])

    def __register_request_received(self, ch, method, properties, body):
        self.logger.debug('register_request_received() - called')
        if self.register_request_processor is not None:
            self.register_request_processor(body)

    def __tweets_callback(self, ch, method, properties, body):
        self.logger.debug('tweets_callback() - called')
        if self.tweets_processor is not None:
            self.tweets_processor(body.decode('utf-8'))

    def __job_to_json(self, job):
        job_dict = {
            'id': job.id,
            'research_object_id': job.research_object_id,
            'filter': job.filter,
            'language': job.language,
            'type': job.type,
            'interval': job.interval,
            'capacity': job.capacity,
            'lastRun': job.lastRun
        }

        return json.dumps(job_dict, default=json_util.default)

    @staticmethod
    def __parse_job(json_message):
        job_instance = ConnectorJob()
        job_instance.id = json_message['id']
        job_instance.filter = json_message['filter']
        job_instance.language = json_message['language']
        job_instance.type = json_message['type']
        job_instance.interval = json_message['interval']
        job_instance.capacity = json_message['capacity']
        job_instance.lastRun = json_message['lastRun']
        job_instance.research_object_id = str(json_message['research_object_id'])

        return job_instance

    def __connect(self):
        # lock with double checking:
        if self.connection is None:
            self.start_lock.acquire()
            if self.connection is None:
                self.connection = pika.BlockingConnection(pika.ConnectionParameters(self.rabbitmq_host))
                self.channel = self.connection.channel()
                self.logger.info('connect() - connected to channel')

                # creating queue if it's not exists:
                self.channel.queue_declare(queue=REGISTRATION_RESPONSE_QUEUE)
                self.channel.queue_declare(queue=CONNECTOR_JOB_QUEUE)
                self.channel.queue_declare(queue=REGISTRATION_QUEUE_NAME)
                self.channel.queue_declare(queue=KEEP_ALIVE_QUEUE)
                self.channel.queue_declare(queue=TWEETS_QUEUE)

                # registration for consuming
                self.channel.basic_consume(self.__register_callback,
                                           queue=REGISTRATION_RESPONSE_QUEUE,
                                           no_ack=True)
                self.channel.basic_consume(self.__register_request_received,
                                           queue=REGISTRATION_QUEUE_NAME,
                                           no_ack=True)
                self.channel.basic_consume(self.__job_callback,
                                           queue=CONNECTOR_JOB_QUEUE,
                                           no_ack=True)
                self.channel.basic_consume(self.__keep_alive_callback,
                                           queue=KEEP_ALIVE_QUEUE,
                                           no_ack=True)
                self.channel.basic_consume(self.__tweets_callback,
                                           queue=TWEETS_QUEUE,
                                           no_ack=True)

                self.wait_thread = threading.Thread(target=self.channel.start_consuming)
                self.wait_thread.daemon = True
                self.wait_thread.start()

            self.start_lock.release()
