import logging

from emmy.common.models import TwitterConnector, CommunicationClient, TwitterClient, ConnectorsRepository, Registrar, \
    JobManager, AdminService, DataReceiver, ConnectorJobRepository, AnalyticJobRepository
from emmy.connectors.twitter import TwitterSearchConnector
from emmy.dal.repositories import DbConnectorsRepository, DbJobRepository, DbAnalyticJobRepository
from emmy.dal.twitter import TwitterSearchClient
from emmy.logic.data_handlers import DbDataReceiver
from emmy.logic.managers import JobManagerService
from emmy.logic.registrars import RegistrarService
from emmy.logic.services import CoreAdminService
from emmy.utils.communication import RabbitCommunicationClient


def configure_dependencies(binder):
    """
    To resgister new dependency you need to add
    binding here
    :param binder: inject framework input
    :return:
    """
    logging.debug('configure_dependencies() - method called')
    binder.bind(CommunicationClient, RabbitCommunicationClient())
    binder.bind(TwitterConnector, TwitterSearchConnector())
    binder.bind(TwitterClient, TwitterSearchClient())
    binder.bind(ConnectorsRepository, DbConnectorsRepository())
    binder.bind(AnalyticJobRepository, DbAnalyticJobRepository())
    binder.bind(Registrar, RegistrarService())
    binder.bind(ConnectorJobRepository, DbJobRepository())
    binder.bind(JobManager, JobManagerService())
    binder.bind(AdminService, CoreAdminService())
    binder.bind(DataReceiver, DbDataReceiver())
