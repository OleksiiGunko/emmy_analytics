import configparser
import logging
import socket

from emmy.common.enums import NodeType, ConnectorType

EMMY_CONNECTOR_LOG = 'logs/emmy_connector.log'

TWITTER_API_SECTION = "TwitterApi"
REDIS_CONFIGURATION_SECTION = "RedisConfiguration"
CONNECTOR_INFO_SECTION = "ConnectorInfo"
CONFIG_PATH = "etc/config.ini"
NODE_SECTION = "NodeInfo"


class ConfigManager(object):
    """
    Responsible for read-writer operation with configuration
    Singleton class
    """
    class __ConfigManager:

        def __init__(self):
            self.logger = logging.getLogger(__name__)
            self.logger.debug('ConfigManager - created')
            self.configParser = configparser.ConfigParser()

        def load_config(self):
            """
            Loading configuration info from INI file
            :return: True - if read operation completed
            """
            try:
                self.configParser.read(CONFIG_PATH)
                self.logger.info('loadConfig - configuration read, sections: %s',
                                 self.configParser.sections())
                return True
            except FileExistsError:
                self.logger.error("Can't find config file %s", CONFIG_PATH)
                return False
            except Exception as exc:
                self.logger.error("Error on configuration load: %s", exc)
                return False

        def get_node_type(self):
            """
            Returns node type
            :return:
            """
            type_val = self.configParser.get(NODE_SECTION, "Type")
            if type_val.lower() == "connector":
                return NodeType.CONNECTOR
            if type_val.lower() == "admin":
                return NodeType.ADMIN
            if type_val.lower() == "all":
                return NodeType.ALL

            return NodeType.UNKNOWN

        def get_connector_type(self):
            """
            Returns connector type
            :return:
            """
            connector_type_val = self.configParser.get(CONNECTOR_INFO_SECTION, "Type")

            if connector_type_val.lower() == "twittersearch":
                return ConnectorType.TWITTER_SEARCH

            return ConnectorType.UNKNOWN

        def get_rabbitmq_host(self):
            """
            Returns hostname of rebbit mq
            :return: host
            """
            return self.configParser.get(REDIS_CONFIGURATION_SECTION, "Hostname")

        def get_mongo_uri(self):
            """
            Returns mongo uri from config
            :return:
            """
            return self.configParser.get("MongoInfo","Uri")

        def get_monhadoop_folder(self):
            mongo_hadoop_folder = self.configParser.get('Admin', 'MongoHadoop')
            return "{}/spark/src/main/python".format(mongo_hadoop_folder)

        def get_local_hostname(self):
            self.logger.debug('get_local_hostname() - method called')
            return socket.gethostname()

        def getDataFolder(self):
            return self.configParser.get('Admin','DataFolder')

        def getOutputFolder(self):
            return self.configParser.get('Admin','OutputFolder')

        def getHadoopUser(self):
            return self.configParser.get('Admin', 'HadoopUser')

        def getHadoopHost(self):
            return self.configParser.get('Admin','HadoopHost')

        def getHadoopPort(self):
            return self.configParser.get('Admin','HadoopPort')

        def get_access_token(self):
            access_token = self.configParser.get(TWITTER_API_SECTION, "Access_Token")
            self.logger.debug("get_access_token() - %s", access_token)
            return access_token

        def get_access_secret(self):
            secret = self.configParser.get(TWITTER_API_SECTION, "Access_Secret")
            self.logger.debug('get_access_secret() - %s', secret)
            return secret

        def get_consumer_key(self):
            key = self.configParser.get(TWITTER_API_SECTION, "Consumer_Key")
            self.logger.debug('get_consumer_key() - %s', key)
            return key

        def get_consumer_secret(self):
            secret = self.configParser.get(TWITTER_API_SECTION, "Consumer_Secret")
            self.logger.debug('get_consumer_secret() - %s', secret)
            return secret

        def use_hdfs(self):
            return self.configParser.getboolean("Admin", "UseHdfs")

        def get_spark_location(self):
            return self.configParser.get('Admin','SparkFolder')

    instance = None

    def __init__(self):
        if not ConfigManager.instance:
            ConfigManager.instance = ConfigManager.__ConfigManager()

    def __getattr__(self, item):
        return getattr(self.instance, item)

    def __setattr__(self, key, val):
        return setattr(self.instance, key, val)
