import json
import os
import re

import sys

import datetime

from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.ml import Pipeline, PipelineModel
import pymongo_spark


# smiles
emo_repl = {
    # positive emoticons
    "&lt;3": " good ",
    ":d": " good ",  # :D in lower case
    ":dd": " good ",  # :DD in lower case
    "8)": " good ",
    ":-)": " good ",
    ":)": " good ",
    ";)": " good ",
    "(-:": " good ",
    "(:": " good ",
    # negative emoticons:
    ":/": " bad ",
    ":&gt;": " sad ",
    ":')": " sad ",
    ":-(": " bad ",
    ":(": " bad ",
    ":S": " bad ",
    ":-S": " bad ",
}

# abreviatures
re_repl = {
    r"\br\b": "are",
    r"\bu\b": "you",
    r"\bhaha\b": "ha",
    r"\bhahaha\b": "ha",
    r"\bdon't\b": "do not",
    r"\bdoesn't\b": "does not",
    r"\bdidn't\b": "did not",
    r"\bhasn't\b": "has not",
    r"\bhaven't\b": "have not",
    r"\bhadn't\b": "had not",
    r"\bwon't\b": "will not",
    r"\bwouldn't\b": "would not",
    r"\bcan't\b": "can not",
    r"\bcannot\b": "can not",
}

def get_text(line):
    """
    Preprocessing test for prediction:
    removing hyperlinks, replacings smiles, etc
    :param line:
    :return:
    """
    json_msg = json.loads(line)
    text = json_msg['text']

    clear_text = text.lower()
    clear_text = re.sub(r'\n','',clear_text)
    clear_text = re.sub(r'rt\s+', '', clear_text)
    clear_text = re.sub(r'\s+@w+', '', clear_text)
    clear_text = re.sub(r'@\w+', '', clear_text)
    clear_text = re.sub(r'\s+#\\w+', '', clear_text)
    clear_text = re.sub(r'#\w+', '', clear_text)
    clear_text = re.sub(r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', '', clear_text)
    clear_text = clear_text.replace('&amp;','&')

    # wa with join and filter for python 3
    #r = re.compile('^[a-zA-Z]+$')
    #clear_text = "".join(filter(r.match, clear_text))

    emo_repl_order = [k for (k_len, k) in reversed(
        sorted([(len(k), k) for k in list(emo_repl.keys())]))]

    # replacing smiles
    for smile in emo_repl_order:
        clear_text = clear_text.replace(smile, emo_repl[smile])

    for r, repl in re_repl.items():
        clear_text = re.sub(r, repl, clear_text)

    return 1, clear_text

def main():
    """
    Loading data from folder, preparing and clearing it
    and make a prediction with a model
    :return:
    """
    # parsing arguments
    print(sys.argv)

    folder_to_process = sys.argv[1]
    res_obj_id = sys.argv[2]
    output_folder = sys.argv[3]
    channel_type = sys.argv[4]
    language = sys.argv[5]
    mongo_uri = sys.argv[6]
    current_date = datetime.datetime.now().date()

    # creating file strature
    pos_output_folder = "{}/{}/sent/pos/{}/".format(output_folder, res_obj_id, language)
    neg_output_folder = "{}/{}/sent/neg/{}/".format(output_folder, res_obj_id, language)

    # creating folders if not exists
    if not os.path.exists(pos_output_folder):
        os.makedirs(pos_output_folder)
    if not os.path.exists(neg_output_folder):
        os.makedirs(neg_output_folder)

    spark = SparkSession.builder.getOrCreate()
    sc = SparkContext.getOrCreate()

    pymongo_spark.activate()

    model = PipelineModel.load('./models/sent_model')
    text_to_predict = sc.textFile(folder_to_process).cache()
    tweets_sequence = text_to_predict.map(lambda l: get_text(l))

    prediction_df = spark.createDataFrame(tweets_sequence).toDF('label', 'sentence')
    pred_results = model.transform(prediction_df).select('label', 'sentence', 'prediction', 'probability').collect()

    pos_counter = neg_counter = 0
    for row in pred_results:
        # saving results in files
        if row.prediction == 1.0:
            pos_counter += 1
            with open('pos.txt','a') as f:
                f.write(row.sentence + '\n')
        else:
            neg_counter += 1
            with open('neg.txt', 'a') as f:
                f.write(row.sentence + '\n')

    # saving result to mongo db
    analytic_result = sc.parallelize([
        {'research_object_id':res_obj_id,
        'execution_date': current_date.strftime('%m-%d-%Y'),
        'positive': str(pos_counter),
        'negative': str(neg_counter),
        'type': channel_type,
        'language': language }
    ])

    db_uri ="{}emmy_db.sent_analytic_results".format(mongo_uri)
    analytic_result.saveToMongoDB(db_uri)

if __name__ == "__main__":
    print('Starting Sentimental Analysis job')
    main()
    # script to run with python 3:
    # ~/Soft/spark-2.1.0-bin-hadoop2.7$ bin/spark-submit --master local[2] predict_sent.py /home/oleksii/data_folder/en/2017-03-14/Samsung_tweets.txt 1 /home/oleksii/data_folder/en >> out.txt
