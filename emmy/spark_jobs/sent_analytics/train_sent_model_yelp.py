import re

from pyspark import SparkContext
from pyspark.ml.classification import NaiveBayes
from pyspark.sql import SparkSession
from pyspark.ml.feature import HashingTF, Tokenizer, IDF, RegexTokenizer
from pyspark.ml import Pipeline
from pyspark.ml.feature import NGram
from pyspark.ml.feature import StopWordsRemover
from os.path import isfile, join

# smiles
emo_repl = {
    # positive emoticons
    "&lt;3": " good ",
    ":d": " good ",  # :D in lower case
    ":dd": " good ",  # :DD in lower case
    "8)": " good ",
    ":-)": " good ",
    ":)": " good ",
    ";)": " good ",
    "(-:": " good ",
    "(:": " good ",
    # negative emoticons:
    ":/": " bad ",
    ":&gt;": " sad ",
    ":')": " sad ",
    ":-(": " bad ",
    ":(": " bad ",
    ":S": " bad ",
    ":-S": " bad ",
}

# abreviatures
re_repl = {
    r"\br\b": "are",
    r"\bu\b": "you",
    r"\bhaha\b": "ha",
    r"\bhahaha\b": "ha",
    r"\bdon't\b": "do not",
    r"\bdoesn't\b": "does not",
    r"\bdidn't\b": "did not",
    r"\bhasn't\b": "has not",
    r"\bhaven't\b": "have not",
    r"\bhadn't\b": "had not",
    r"\bwon't\b": "will not",
    r"\bwouldn't\b": "would not",
    r"\bcan't\b": "can not",
    r"\bcannot\b": "can not",
}

def create_sequence(text):
    """
    preprocessing function
    :param sentence:
    :param type:
    :return:
    """
    label = text[len(text)-1]
    text = text[:len(text)-1]
    # remove hyperlinks
    text = re.sub(r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', '', text, flags=re.MULTILINE)

    emo_repl_order = [k for (k_len, k) in reversed(
        sorted([(len(k), k) for k in list(emo_repl.keys())]))]
    text = text.lower()
    # replacing smiles
    for smile in emo_repl_order:
        text = text.replace(smile, emo_repl[smile])

    for r, repl in re_repl.items():
        text = re.sub(r, repl, text)

    text = text.replace("-", " ").replace("_", " ")

    return int(label), text

if __name__=="__main__":
    spark = SparkSession.builder.getOrCreate()
    sc = SparkContext.getOrCreate()
    print('Starting Emmy Job')

    #all_data = sc.textFile('/home/oleksii/ML/DataSets/sentimental_labelled_sent/amazon_cells_labelled.txt').cache()
    all_data = sc.textFile('/home/oleksii/ML/DataSets/sentimental_labelled_sent/full.txt').cache()

    train_neg = all_data.map(lambda file: create_sequence(file))
    full_df = spark.createDataFrame(train_neg).toDF('label','sentence')

    tokenizer = Tokenizer(inputCol="sentence", outputCol="tokens")
    ngram = NGram(n=1, inputCol="tokens", outputCol="ngram")
    rusStopWords = StopWordsRemover.loadDefaultStopWords('english')
    remover = StopWordsRemover(inputCol="ngram", outputCol="filtered").setStopWords(rusStopWords)

    hashingTF = HashingTF(inputCol="filtered", outputCol="features_tf")
    idf = IDF(inputCol='features_tf', outputCol='features')

    bayesClassifier = NaiveBayes(smoothing=1.0, modelType="multinomial")
    pipeline = Pipeline(stages=[tokenizer, ngram, remover,hashingTF, idf, bayesClassifier])

    training, test = full_df.randomSplit([0.8, 0.2])

    model = pipeline.fit(training)
    prediction = model.transform(test)

    selected = prediction.select('label','sentence','probability','prediction')

    counter = 0
    for row in selected.collect():
        lbl, sentence, prob, predic = row
        print('({}, {}) --> prob={}, pred = {}'.format (lbl, sentence.encode('utf-8'), str(prob), predic))
        if lbl == predic:
            counter+=1
        else:
            print('=== {}'.format(sentence.encode('utf-8')))

    accuracy = 1.0 * counter / test.count()
    print('model accuracy - {}, correct num = {}, all - {}'.format(accuracy, counter, test.count()))
    model.save('sent_model')
    sc.stop()