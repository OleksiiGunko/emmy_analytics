import logging
import threading
import time

import datetime

from emmy.common.enums import ConnectorStatus
from emmy.common.models import TwitterConnector
from emmy.utils.config import ConfigManager

REGISTRATION_TIMEOUT = 20


class TwitterSearchConnector(TwitterConnector):
    """
    Responsible for collecting data from Twitter Search API
    and sending it for processing
    """

    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.logger.info('TwitterSearchConnector() - created')
        self.isRegistered = False
        self.listening_thread = None
        self.keep_alive_thread = None
        self.event_interrupt_listening = threading.Event()
        self.event_interrupt_listening.clear()
        self.config_manager = ConfigManager()
        self.local_host = None
        self.status = ConnectorStatus.UNKNOWN
        self.active_job = None

    def __registered_callback(self, hostname):
        """
        Handling registration response from admin
        :param hostname:
        :return:
        """
        self.logger.debug('registered_callback() - response received - %s', hostname)
        if hostname.decode('utf-8') == self.local_host:
            self.logger.info('registered_callback() - registration done')
            self.isRegistered = True

    def __job_callback(self, job):
        """
        Handling job from admin
        :param job: Job object from admin
        :return:
        """
        self.logger.debug('job_callback() - job received - %s', job)
        if self.status == ConnectorStatus.BUSY:
            self.logger.warning('job_callback() - connector is busy, cant start new job')
            return

        self.active_job = job
        # starting job thread
        self.job_worker = threading.Thread(target=self.__job_worker,
                                           args=(job,))
        self.job_worker.start()
        self.status = ConnectorStatus.BUSY

    def __job_worker(self, job):
        """
        Main job executor
        :param job:
        :return:
        """
        self.logger.debug('job_worker() - running job %s', job.research_object_id)

        # trying while not connect
        while not self.api_client.connect():
            self.logger.warning('job_worker() - cant connect to twitter')
            time.sleep(10)

        # getting only latest tweets
        today_filter = "{} since:{}".format(job.filter, datetime.datetime.now().date())
        tweets = self.api_client.get_tweets_array(search_filter= today_filter, count=job.capacity, lang=job.language)

        if tweets is None:
            self.logger.debug('job_worker() - no tweets received')
        if not self.comm_client.send_collected_tweets(job.research_object_id, tweets):
            self.logger.warning('job_worker() - cant send tweets')
        else:
            self.logger.debug('job_worker() - tweets sent to admin')
        self.status = ConnectorStatus.READY

    def __register(self):
        """
        Starts registration to emmy admin
        :return: True if registered
        """
        self.logger.info('register() - starting registration')
        self.local_host = self.config_manager.get_local_hostname()
        self.comm_client.set_regresp_callback(self.__registered_callback)

        if not self.comm_client.send_register_connector(self.local_host):
            self.logger.error('register() - cant send message')
            return False

        # waiting for response from admin
        counter = REGISTRATION_TIMEOUT
        while not self.isRegistered:
            if counter is 0:
                self.logger.error('register() - no response from admin in 20 seconds')
                return False
            counter -= 1
            time.sleep(1)

        self.status = ConnectorStatus.READY
        return True

    def __job_listen_worker(self, interrupt_event):
        """
        Listening for jobds from admin
        :param interrupt_event:
        :return:
        """
        self.logger.info('job_listen_worker() - starting listening')
        self.comm_client.start_job_listening(self.__job_callback)
        interrupt_event.wait()
        self.comm_client.stop()

    def __keep_alive_worker(self):
        """
        methods sends connector status to admin service
        :return: None
        """
        self.logger.debug('keep_alive_worker() - started')
        while self.isRegistered:
            self.comm_client.send_connector_status(self.local_host, self.status)
            if self.event_interrupt_listening.is_set():
                self.logger.info('keep_alive_worker() - stopping worker')
                break
            else:
                time.sleep(20)

    def start(self):
        """
        Registering and starts listening thread
        :return:
        """
        self.logger.info('start() - starting connector')

        try:
            self.comm_client.start()

            # listening thread that waiting for tasks
            self.listening_thread = threading.Thread(target=self.__job_listen_worker,
                                                     args=(self.event_interrupt_listening,))
            self.listening_thread.start()

            while not self.isRegistered:
                if not self.__register():
                    self.logger.warning('start() - no register response, will try in 20 seconds')
                    time.sleep(20)

            # keep alive thread that sends connector status
            self.keep_alive_thread = threading.Thread(target=self.__keep_alive_worker(),
                                                      args=(self.event_interrupt_listening,))
            self.keep_alive_thread.start()

            self.logger.info('start() - started all threads')
            return True

        except Exception as exc:
            self.logger.error('start() - error occurred: %s', exc)
            return False

    def stop(self):
        """
        Stopping listening thread
        :return: True is ok
        """
        self.logger.debug('stop() - stopping connector')
        try:
            self.event_interrupt_listening.set()
            self.isRegistered = False
            self.listening_thread.join()
            self.logger.debug('stop() - listening thread stoped')
            self.keep_alive_thread.join()
            self.logger.info('stop() - connector stoped')
            return True
        except Exception as exc:
            self.logger.error('stop() - error on stop: %s', exc)
            return False
