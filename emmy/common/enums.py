from enum import IntEnum


class ConnectorType(IntEnum):
    UNKNOWN = 0,
    TWITTER_SEARCH = 1


class NodeType(IntEnum):
    UNKNOWN = 0,
    CONNECTOR = 1,
    ADMIN = 2,
    ALL = 3


class ConnectorStatus(IntEnum):
    UNKNOWN = 0,
    READY = 1,
    BUSY = 2,
    ERROR = 3


class JobStatus(IntEnum):
    UNKNOWN = 0,
    PENDING = 1,
    IN_PROGRESS = 2,
    DONE = 3,
    ERROR = 4

class AnalyticJobType(IntEnum):
    SENTIMENT_ANALYTICS = 0,
    LDA = 1
