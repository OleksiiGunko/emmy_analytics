import inject


class Config(object):
    pass


class ResearchObject:
    """
    This object contains general info about
    object of our research and analytics job
    """
    id = None
    name = None
    twitter_search = False
    facebook_search = False
    search_keywords = None


class CommunicationClient(object):
    config = inject.attr(Config)


class TwitterClient(object):
    config = inject.attr(Config)


class TwitterConnector(object):
    config = inject.attr(Config)
    comm_client = inject.attr(CommunicationClient)
    api_client = inject.attr(TwitterClient)


class ConnectorJob(object):
    config = inject.attr(Config)

    def __init__(self):
        self.id = None
        self.type = None
        self.language = None
        self.filter = None
        self.interval = None
        self.lastRun = None
        self.capacity = None
        self.connector_job = None
        self.research_object_id = None


class AnalyticJob(object):
    config = inject.attr(Config)

    def __init__(self):
        self.id = None
        self.research_object_id = None
        self.name = None
        self.input_folder = None
        self.language = None
        self.status = None
        self.type = None
        self.connector_type = None


class ConnectorsRepository(object):
    config = inject.attr(Config)


class ConnectorJobRepository(object):
    config = inject.attr(Config)


class AnalyticJobRepository(object):
    config = inject.attr(Config)


class Registrar(object):
    config = inject.attr(Config)
    connectors_rep = inject.attr(ConnectorsRepository)
    com_client = inject.attr(CommunicationClient)


class JobManager(object):
    config = inject.attr(Config)
    com_client = inject.attr(CommunicationClient)
    cjobs_repository = inject.attr(ConnectorJobRepository)
    ajobs_repository = inject.attr(AnalyticJobRepository)
    connector_repository = inject.attr(ConnectorsRepository)


class DataReceiver(object):
    config = inject.attr(Config)
    com_client = inject.attr(CommunicationClient)
    cjob_repository = inject.attr(ConnectorJobRepository)
    ajob_repository = inject.attr(AnalyticJobRepository)


class AdminService(object):
    config = inject.attr(Config)
    registrar = inject.attr(Registrar)
    data_receiver = inject.attr(DataReceiver)
    job_manager = inject.attr(JobManager)
    com_client = inject.attr(CommunicationClient)


class Connector():
    """
    Model for connector
    """

    def __init__(self):
        self.id = None
        self.hostname = None
        self.status = None
