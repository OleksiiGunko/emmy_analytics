import datetime
from pymongo import MongoClient
import csv


def add_jobs():
    """
    Adding standart jobs to db
    :return:
    """
    print('adding jobs')

    mongo_client = MongoClient('localhost', 27017)
    db = mongo_client.emmy_db
    # getting list from file:
    with open('electronic.txt') as csvfile:
        reader = csv.reader(csvfile, delimiter=':')
        for row in reader:
            print('Adding new research object: ' + row[0])
            # 1. - adding research object:
            res_obj = {
                'name': row[0],
                'twitter_search': True,
                'facebook_search': True,
                'search_keyword': row[1]
            }
            db.research_objects.remove({})
            obj_id = db.research_objects.insert_one(res_obj).inserted_id
            print('Object added, id - {}'.format(obj_id))

            # 2 . adding connector object
            conn_job = {
                'research_object_id': str(obj_id),
                'type': 'TwitterSearch',
                'language': 'en',
                'lastRun': datetime.datetime.now() - datetime.timedelta(1, ),
                'capacity': 500,
                'interval': 60 * 60 * 12,  # 12 hour interval
                'filter': row[1]
            }

            db.connector_jobs.remove({})
            id = db.connector_jobs.insert_one(conn_job).inserted_id
            print("Connector job added - {}".format(id))

    mongo_client.close()
    print('Finished')


def main():
    print('Start adding initial data')
    add_jobs()


if __name__=="__main__":
    main()