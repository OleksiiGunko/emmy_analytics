import json
import threading

import pika

TWEETS_QUEUE = "tweets_queue"

class Communicator:
    def __init__(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters("localhost"))
        self.connector_host = None

    def callback(self, ch, method, properties, body_txt):
        print('*** Register request received - %r' % (body_txt,))
        self.connector_host = (body_txt,)

        self.channel.basic_publish(exchange='',
                                   routing_key='registration_response',
                                   body=body_txt)
        print('*** Register response send')

        # creating test job
        self.channel.queue_declare('connector_job_queue')


        test_json = {
            "id": 'test_job',
            "filter": 'samsung OR nokia OR iphone since:2015-03-07',
            "language": 'en',
            "status": 'running',
            "type": "twitter_search",
            "lastRun": "12:00:00",
            "stop_time": "None",
            "capacity": 500,
            "interval": 60*60
        }

        message = json.dumps(test_json)

        print('*** Sending new test job: ' + message)
        self.channel.basic_publish(exchange="",
                                   routing_key="connector_job_queue",
                                   body=message)
        print('*** Job was sent to conenctor')

    def callback_tweets(self, ch, method, properties, body):
        print('callback_tweets() - tweets received')
        with open('tweets.txt', 'a') as f:
            f.write(body.decode('utf-8'))
            f.write('\n')

    def keep_alive_processor(self, ch, method, properties, body):
        print('--- Keep alive received - %r' % (body,))

    def start(self):
        self.channel = self.connection.channel()

    def consume_registration(self):
        self.channel.queue_declare('registration')
        self.channel.queue_declare(TWEETS_QUEUE)

        self.channel.basic_consume(self.callback,
                                   queue="registration",
                                   no_ack=True)

        self.channel.basic_consume(self.callback_tweets,
                                   queue=TWEETS_QUEUE,
                                   no_ack=True)
        print('*** Listening for registration request')
        wait_thread = threading.Thread(target=self.channel.start_consuming)
        wait_thread.daemon = True
        wait_thread.start()

    def listen_keepalive(self):
        queue_name = "keep_alive"
        self.channel.queue_declare(queue=queue_name)
        self.channel.basic_consume(self.keep_alive_processor,
                                   queue=queue_name,
                                   no_ack=True)

    def close(self):
        self.connection.close()


if __name__ == "__main__":
    print('Starting Admin Simulator')
    print('________________________')
    comm = Communicator()
    comm.start()
    comm.consume_registration()
    comm.listen_keepalive()

    inp = input('To STOP simulator press enter\n')
    comm.close()
