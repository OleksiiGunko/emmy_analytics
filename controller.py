import logging
import signal
import sys
import time
from logging.handlers import RotatingFileHandler

import inject
from emmy.common.models import TwitterConnector, AdminService

from emmy.common.enums import NodeType, ConnectorType
from emmy.utils.config import ConfigManager
from emmy.utils.dependency_mapper import configure_dependencies

workers_list = []

def sigterm_handler(signum, frame):
    print('terminated signal called')
    for worker in workers_list:
        worker.stop()
    exit(0)

def main(args):
    """
    Main program function
    :param args: input arguments
    :return: None
    """
    print('Starting EmmyAnalytics node controller')
    print('Input args - ' + str(args))

    signal.signal(signal.SIGTERM, sigterm_handler)

    logger = get_logger()
    logger.info('EmmyAnalytics - starting node')

    config_manager = ConfigManager()
    while config_manager.load_config() is not True:
        logger.error('Cant load configuration, will try in 1 minutes')
        time.sleep(60)

    inject.configure(configure_dependencies)
    run_node_workers(config_manager.get_node_type())
    logger.info('Node workers started')


def run_node_workers(node_type):
    """
    Creates worker according to node type
    :return:
    """
    if node_type == NodeType.ADMIN or node_type == NodeType.ALL:
        print('Starting admin service')
        admin_service = inject.instance(AdminService)
        workers_list.append(admin_service)

    if node_type == NodeType.CONNECTOR or node_type == NodeType.ALL:
        print('Starting connector')
        connector_type = ConfigManager.instance.get_connector_type()
        connector_worker = get_connector(connector_type)
        workers_list.append(connector_worker)

    for worker in workers_list:
        worker.start()


def get_connector(connector_type):
    """
    Returns instance of connector
    :param connector_type: type of connector
    :return: instance
    """
    if connector_type == ConnectorType.TWITTER_SEARCH:
        return inject.instance(TwitterConnector)

    return None


def get_logger():
    """
    Configuring logging for application
    :return: instance of logger
    """
    logging.basicConfig(level=logging.DEBUG, filename='logs/emmy_log.txt' ,format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    logger = logging.getLogger("node_controller")

    return logger


if __name__ == "__main__":
    main(sys.argv)
