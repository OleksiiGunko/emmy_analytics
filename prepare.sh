#!/bin/bash -e

echo "Installing dependencies for EmmyAnalytics"
sudo pip install -r requirements.txt
echo "All dependent packages installed"