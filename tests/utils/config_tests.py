import logging
import unittest

from emmy.common.enums import NodeType
from emmy.utils.config import ConfigManager


class ConfigManagerTests(unittest.TestCase):
    """
    Tests for configuration manager
    """

    def setUp(self):
        print('setUp tests')
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger(__name__)
        logging.getLogger().addHandler(logging.StreamHandler())

    def test_loadConfig(self):
        """
        testing that config can be loaded
        :return:
        """

        #arrange
        configMngr = ConfigManager()

        #act
        res = configMngr.load_config()

        #assert
        self.assertEqual(res, True)

    def test_get_node_type(self):
        """
        Checking connector type
        :return:
        """
        configMngr = ConfigManager()

        configMngr.load_config()
        type = configMngr.get_node_type()

        self.assertEqual(type, NodeType.CONNECTOR)

if __name__ == "__main__":
    unittest.main()