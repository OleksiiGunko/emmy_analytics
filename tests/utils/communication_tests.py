import logging
import unittest

from emmy.utils.config import ConfigManager
from emmy.utils.communication import RabbitCommunicationClient


class RabbitClientTests(unittest.TestCase):
    """
    Testing communication with rabbit mq
    """

    def setUp(self):
        print('setUp tests')
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger(__name__)
        logging.getLogger().addHandler(logging.StreamHandler())
        ConfigManager().load_config()

    def test_send_register(self):
        client = RabbitCommunicationClient()
        res = client.send_register_connector('localhost')
        self.assertTrue(res,'Cant send message')

if __name__ == "__main__":
    unittest.main()