import logging
import unittest

from emmy.logic.services import CoreAdminService
from emmy.utils.config import ConfigManager


class AdminServiceTests(unittest.TestCase):
    """
    Unit tests for admin service tests
    """
    def setUp(self):
        print('setUp tests')
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger(__name__)
        logging.getLogger().addHandler(logging.StreamHandler())
        ConfigManager().load_config()
        self.service = CoreAdminService()
